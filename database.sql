Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris>mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 43
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [(none)]> USE music_db;
Database changed

MariaDB [(none)]> CREATE DATABASE music_db;
Query OK, 1 row affected (0.007 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> DROP DATABASE music_db;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [(none)]> CREATE DATABASE music_db;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> USE music_db;
Database changed
MariaDB [music_db]> CREATE TABLE artists (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> name VARCHAR(50) NOT NULL,
    -> PRIMARY KEY (id)
    -> );
Query OK, 0 rows affected (0.971 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| artists            |
+--------------------+
1 row in set (0.001 sec)

MariaDB [music_db]> DESCRIBE artists;
+-------+-------------+------+-----+---------+----------------+
| Field | Type        | Null | Key | Default | Extra          |
+-------+-------------+------+-----+---------+----------------+
| id    | int(11)     | NO   | PRI | NULL    | auto_increment |
| name  | varchar(50) | NO   |     | NULL    |                |
+-------+-------------+------+-----+---------+----------------+
2 rows in set (0.145 sec)

MariaDB [music_db]> CREATE TABLE users (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> username VARCHAR(50) NOT NULL,
    -> password VARCHAR(50) NOT NULL,
    -> PRIMARY KEY (id)
    -> );
Query OK, 0 rows affected (0.429 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| artists            |
| users              |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE users;
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| username | varchar(50) | NO   |     | NULL    |                |
| password | varchar(50) | NO   |     | NULL    |                |
+----------+-------------+------+-----+---------+----------------+
3 rows in set (0.031 sec)

Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris>mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 66
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [(none)]> USE music_db;
Database changed

MariaDB [music_db]> CREATE TABLE albums (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> name VARCHAR(50) NOT NULL,
    -> year DATE NOT NULL,
    -> artist_id INT NOT NULL,
    -> PRIMARY KEY (id),
    -> CONSTRAINT fk_albums_artist_id
    -> FOREIGN KEY (artist_id)
    -> REFERENCES artists (id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (0.840 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| users              |
+--------------------+
3 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE albums;
+-----------+-------------+------+-----+---------+----------------+
| Field     | Type        | Null | Key | Default | Extra          |
+-----------+-------------+------+-----+---------+----------------+
| id        | int(11)     | NO   | PRI | NULL    | auto_increment |
| name      | varchar(50) | NO   |     | NULL    |                |
| year      | date        | NO   |     | NULL    |                |
| artist_id | int(11)     | NO   | MUL | NULL    |                |
+-----------+-------------+------+-----+---------+----------------+
4 rows in set (0.024 sec)

MariaDB [music_db]> CREATE TABLE playlists (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> date_created DATETIME NOT NULL,
    -> user_id INT NOT NULL,
    -> PRIMARY KEY (id),
    -> CONSTRAINT fk_playlists_user_id
    -> FOREIGN  KEY (user_id)
    -> REFERENCES users (id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (1.088 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| users              |
+--------------------+
4 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE playlists;
+--------------+----------+------+-----+---------+----------------+
| Field        | Type     | Null | Key | Default | Extra          |
+--------------+----------+------+-----+---------+----------------+
| id           | int(11)  | NO   | PRI | NULL    | auto_increment |
| date_created | datetime | NO   |     | NULL    |                |
| user_id      | int(11)  | NO   | MUL | NULL    |                |
+--------------+----------+------+-----+---------+----------------+
3 rows in set (0.165 sec)

MariaDB [music_db]> CREATE TABLE songs (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> title VARCHAR(50) NOT NULL,
    -> length TIME NOT NULL,
    -> genre VARCHAR(50) NOT NULL,
    -> album_id INT NOT NULL,
    -> PRIMARY KEY (id),
    -> CONSTRAINT fk_songs_album_id
    -> FOREIGN KEY(album_id) REFERENCES albums (id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (1.021 sec)

MariaDB [music_db]> CREATE TABLE playlists_songs (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> playlist_id INT NOT NULL,
    -> song_id INT NOT NULL,
    -> PRIMARY KEY (id),
    -> CONSTRAINT fk_playlists_songs_playlist_id
    -> FOREIGN KEY (playlist_id) REFERENCES playlists(id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT,
    -> CONSTRAINT fk_playlists_songs_song_id
    -> FOREIGN KEY (song_id) REFERENCES songs(id)
    -> ON UPDATE CASCADE
    -> ON DELETE RESTRICT
    -> );
Query OK, 0 rows affected (1.290 sec)

MariaDB [music_db]> SHOW TABLES;
+--------------------+
| Tables_in_music_db |
+--------------------+
| albums             |
| artists            |
| playlists          |
| playlists_songs    |
| songs              |
| users              |
+--------------------+
6 rows in set (0.001 sec)

MariaDB [music_db]> DESCRIBE songs;
+----------+-------------+------+-----+---------+----------------+
| Field    | Type        | Null | Key | Default | Extra          |
+----------+-------------+------+-----+---------+----------------+
| id       | int(11)     | NO   | PRI | NULL    | auto_increment |
| title    | varchar(50) | NO   |     | NULL    |                |
| length   | time        | NO   |     | NULL    |                |
| genre    | varchar(50) | NO   |     | NULL    |                |
| album_id | int(11)     | NO   | MUL | NULL    |                |
+----------+-------------+------+-----+---------+----------------+
5 rows in set (0.020 sec)

MariaDB [music_db]> DESCRIBE playlists_songs;
+-------------+---------+------+-----+---------+----------------+
| Field       | Type    | Null | Key | Default | Extra          |
+-------------+---------+------+-----+---------+----------------+
| id          | int(11) | NO   | PRI | NULL    | auto_increment |
| playlist_id | int(11) | NO   | MUL | NULL    |                |
| song_id     | int(11) | NO   | MUL | NULL    |                |
+-------------+---------+------+-----+---------+----------------+
3 rows in set (0.228 sec)

MariaDB [music_db]>